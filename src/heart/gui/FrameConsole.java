package heart.gui;

import heart.gui.heartComponents.*;
import heart.logic.request.*;
import heart.logic.response.*;

import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:31
 */

	// TODO: Kolorowanie składni

public class FrameConsole extends MainWindow {
	static final HeartJTextPane textPane = new HeartJTextPane();
	final HeartJTextField enterField;

	static ArrayList<String> allData = new ArrayList<String>();
	static ArrayList<String> inputData = new ArrayList<String>();
	int keyPosition;

	public FrameConsole() {
		// ustawienie minimalnej wysokości elementu, wypełnienia, marginesów
		setGBCparameter(new GBC(0,3,3,1).setWeight(100,45).setFill(GBC.BOTH).setIpad(550,170));
		setLayout(new BorderLayout(0, 7));
		setBorder(new EmptyBorder(3, 15, 15, 15));

		enterField = new HeartJTextField(">> ");
		enterField.focusChangeDisabled();
		enterField.setFont(new Font("Courier New", Font.PLAIN, 11));

		add(textPane.getScrollPane(), BorderLayout.CENTER);
		add(new HeartHeader("Console"), BorderLayout.NORTH);
		add(enterField, BorderLayout.SOUTH);

		addLogic();

		if(debugMode)
			setBackground(Color.ORANGE);
	}

	static void updateJTestPane() {
		String out = "";
		for(String s : allData) {
			out += ">> " + s + "\n";
		}

		textPane.setText(out);
	}

	static void updateJTestPane(Request req, Response resp) {
		allData.add(req.getRawRequest());

		String temp = resp.getRaw();

		if(temp.length()>150)
			temp = temp.substring(0,150) + " (...)";

		allData.add(temp);

		String out = "";
		for(String s : allData) {
			out += ">> " + s + "\n";
		}

		// TODO: W razie wyrzucenia errora zmienić kolor paska na dole :)

		textPane.setText(out);
	}

	// LOGIC SECTION

	void addLogic() {
		enterField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					inputData.add(enterField.getText().substring(3));
					allData.add(enterField.getText().substring(3));
					enterField.setText(">> ");

					updateJTestPane();
					keyPosition = inputData.size();

					// TODO: wysyłanie i odbiór obiektu
					// TODO: odrzucanie nieprawidłowych składniowo zapytań, np. bez [], bez liter, same spacje
				}

				if(e.getKeyCode() == KeyEvent.VK_UP) {
					if(keyPosition!=0)
						keyPosition--;

					if(keyPosition>=0) {
						enterField.setText(">> " + inputData.get(keyPosition));
					}
				}

				if(e.getKeyCode() == KeyEvent.VK_DOWN) {
					if(keyPosition<= inputData.size()-1) {
						if(keyPosition != allData.size()-1)
							keyPosition++;

						enterField.setText(">> " + inputData.get(keyPosition));
					}
				}
			}
		});
	}
}
