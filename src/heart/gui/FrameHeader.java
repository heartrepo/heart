package heart.gui;

import heart.gui.heartComponents.*;

import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:09
 */

// Nagłówek menu - zawiera m.in przycisk Switch User, przyciski zamykania i minimalizowania

public class FrameHeader extends MainWindow {
	static FrontFrame frame;
	final Point offset = new Point();

	static final HeartJLabel l = new HeartJLabel("HeaRT");

	public FrameHeader(final FrontFrame frame) {
		this.frame = frame;

		// ustawienie maksymalnej wysokości elementu, wypełnienia, marginesów
		setMaxY(15); // wysokość: 30px (liczone podwójnie)
		setFixedSize(getMaxX(),getMaxY()); // stała neizmienny rozmiar
		setGBCparameter(new GBC(0, 0, 3, 1).setWeight(100, 0).setFill(GBC.BOTH).setIpad(getMinX(), getMaxY()));
		setBorder(new EmptyBorder(0, 10, 5, 0));

		setLayout(new BorderLayout());
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				frame.setLocation(e.getLocationOnScreen().x + offset.x, e.getLocationOnScreen().y + offset.y);
			}
		});
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Point mouse = e.getLocationOnScreen();
				Point window = frame.getLocation();

				offset.setLocation(window.x - mouse.x, window.y - mouse.y);
			}
		});

		add(l, BorderLayout.WEST);
		add(new HeartButtonsNavigate(frame).setTerminateOn(), BorderLayout.EAST);

		if(debugMode)
			setBackground(Color.BLACK);
	}

	public static void setHeaderDescription(String userName) {
		l.setText("HeaRT - " + userName);
		l.repaint();
	}
}
