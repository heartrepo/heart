package heart.gui.heartComponents;

import heart.gui.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 10:12
 */

public class HeartButtonsNavigate extends MainWindow {
	Frame frame;
	boolean terminate = false;

	Point windowLocation;
	Dimension windowSize;

	final HeartJButton minimizeButton;
	final HeartJButton maximizeButton;
	final HeartJButton exitButton;

	public HeartButtonsNavigate(final Frame frame) {
		this.frame = frame;

		setLayout(new FlowLayout(FlowLayout.RIGHT));
		setBorder(new EmptyBorder(-5, 0, -5, -5));

		windowLocation = frame.getLocation();
		windowSize = new Dimension(frame.getWidth(), frame.getHeight());

		exitButton = new HeartJButton(HeartJButton.EXIT);
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(terminate) {
					System.exit(0);
				}
				else {
					frame.setVisible(false);
					frame.dispose();
				}
			}
		});

		minimizeButton = new HeartJButton(HeartJButton.MINIMIZE);
		minimizeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setState(Frame.ICONIFIED);
			}
		});

		maximizeButton = new HeartJButton(HeartJButton.MAXIMIZE);
		maximizeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(frame.getExtendedState()==0) {
					windowLocation = frame.getLocation();
					windowSize = new Dimension(frame.getWidth(), frame.getHeight());
					frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
				}
				else {
					frame.setSize(windowSize);
					frame.setLocation(windowLocation);
				}
			}
		});

		add(minimizeButton);
		add(maximizeButton);
		add(exitButton);
	}

	public HeartButtonsNavigate setTerminateOn() {
		terminate = true;

		return this;
	}

	public HeartButtonsNavigate setExitOnly() {
		maximizeButton.setEnabled(false);
		minimizeButton.setEnabled(false);

		return this;
	}
}
