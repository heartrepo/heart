package heart.gui.heartComponents;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 16.02.13
 * Time: 21:05
 */

public class HeartJRadioButton extends JRadioButton {
	public HeartJRadioButton(String text) {
		super(text);

		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		setBorder(null);
	}
}
