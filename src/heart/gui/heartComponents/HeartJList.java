package heart.gui.heartComponents;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:34
 */

public class HeartJList extends JList {
	public HeartJList() {
		setFont(new Font("Microsoft Yi Baiti", Font.PLAIN, 18));
		setBackground(new Color(246, 246, 246));
		setSelectionBackground(new Color(0, 122, 205));
		setSelectionForeground(Color.WHITE);
		setBorder(null);
	}

	public JScrollPane getScrollPane() {
		JScrollPane temp = new JScrollPane(this);
		temp.setBorder(null);
		return temp;
	}
}
