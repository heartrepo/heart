package heart.gui.heartComponents;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 10.02.13
 * Time: 13:42
 */

public class HeartJTextPane extends JTextPane {
	public HeartJTextPane() {
		setFont(new Font("Courier New", Font.PLAIN, 11));
		setEditable(false);
	}

	public JScrollPane getScrollPane() {
		JScrollPane temp = new JScrollPane(this);
		temp.setBorder(null);
		return temp;
	}

	public HeartJTextPane getEditable() {
		setEditable(true);
		return this;
	}
}
