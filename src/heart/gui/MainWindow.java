package heart.gui;

import heart.logic.parsing.CommParser;
import heart.logic.parsing.Parser;
import heart.logic.request.RequestGetModel;
import heart.logic.request.RequestGetlistModel;
import heart.logic.request.RequestHello;
import heart.logic.response.RespType;
import heart.logic.response.ResponseGetModel;
import heart.logic.response.ResponseGetlistModel;
import heart.logic.response.ResponseHello;
import heart.net.ConnectionHandler;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:12
 */

// Nagłówek - w domyśle będzie emulował wygląd paska w systemach innych niż win
// potrzebuje parametru this do zarządzania minimalizowaniem i zamykaniem okna

public abstract class MainWindow extends JPanel {
	// maksymalne i minimalne wymiary okna (w razie gdyby przyszło nam obsługiwać rozdzielczości typu wide)
	private int maxX = 2000;
	private int maxY = 2000;
	private int minX = 0;
	private int minY = 0;

	//Standardowy kolor JPanela
	private Color mainColor = new Color(239,238,243);
	// Obiekt GBC sterujący wyglądem każdego JPanela dziedziczącego
	private GBC GBCparameter;

	// Pola ikon
	static private Image ornamentHorizontal;
	static private Image ornamentVertical;
	static private Image breakOrnament;
	static private ImageIcon exitIcon;
	static private ImageIcon minimizeIcon;
	static private ImageIcon maximizeIcon;
	static private ImageIcon exitIconOn;
	static private ImageIcon minimizeIconOn;
	static private ImageIcon maximizeIconOn;
	static private ImageIcon minimizeIconOff;
	static private ImageIcon maximizeIconOff;
	static private ImageIcon searchIcon;
	static private ImageIcon buttonIcon;
	static private ImageIcon arrowIcon;

	// WAŻNE!! Debug mode - po ustawieniu na true obsługuje debugowanie zasięgu i wyglądu okien dziedziczących
	boolean debugMode = false;

	// Parser i ConnectionHandler

    public static CommParser myParser;
	public static ConnectionHandler connectionHandler;

	// Modele i Użytkownicy

	static public int currentModelNr = 0;
	static public int currentUserNr = 0;
	static public String currentUser;
	static public LinkedHashMap<String, Vector<String>> usersAndModels;

	static {
        myParser = new Parser();

		try {
			breakOrnament = ImageIO.read(new File("icons\\break.jpg"));
			ornamentHorizontal = ImageIO.read(new File("icons\\ornamentHorizontal.jpg"));
			ornamentVertical = ImageIO.read(new File("icons\\ornamentVertical.jpg"));
			searchIcon = new ImageIcon("icons\\search.jpg");
			buttonIcon = new ImageIcon("icons\\ornamentVertical.jpg");
			arrowIcon = new ImageIcon("icons\\arrow.gif");
			exitIcon = new ImageIcon("icons\\exit.jpg");
			maximizeIcon = new ImageIcon("icons\\maximize.jpg");
			minimizeIcon = new ImageIcon("icons\\minimize.jpg");
			exitIconOn = new ImageIcon("icons\\exitOn.jpg");
			maximizeIconOn = new ImageIcon("icons\\maximizeOn.jpg");
			minimizeIconOn = new ImageIcon("icons\\minimizeOn.jpg");
			maximizeIconOff = new ImageIcon("icons\\maximizeOff.jpg");
			minimizeIconOff = new ImageIcon("icons\\minimizeOff.jpg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MainWindow() {
		setBackground(mainColor);
	}

	public void updateCurrentUser() throws IOException {
		FrameHeader.setHeaderDescription(currentUser);

		ModelAddContent.userName1Text.setText(currentUser);
		ModelAddContent.userName2Text.setText(currentUser);

		FrameModelExplorer.modelList.setListData(usersAndModels.get(currentUser));

		if(!usersAndModels.get(currentUser).isEmpty())
			currentModelNr = 0;

		RequestHello hello = new RequestHello(currentUser);
		ResponseHello respHello = (ResponseHello) connectionHandler.sendAndGet(hello, RespType.HELLO);

		FrameConsole.updateJTestPane(hello, respHello);
	}

	public void updateCurrentModel() throws IOException {
		if(currentModelNr != -1) {
			String modelname = usersAndModels.get(currentUser).get(currentModelNr);

			FrameModelExplorer.modelList.setSelectedIndex(currentModelNr);

			ModelAddContent.modelName2Text.setText(modelname);

			LinkedList<String> tempList = new LinkedList<String>();
			tempList.add("tbls");
			RequestGetModel getModel = new RequestGetModel(currentUser, modelname, "hml", tempList);
			ResponseGetModel respGetModel = (ResponseGetModel) connectionHandler.sendAndGet(getModel, RespType.MODEL_GET);

			FrameConsole.updateJTestPane(getModel, respGetModel);

			ModelAddContent.content2Text.setText(respGetModel.getModelContent());
		}
	}

	public boolean establishConnection(String host, String port) throws IOException {
		try {
			connectionHandler = new ConnectionHandler(host, new Integer(port), new Parser());
			return true;
		} catch(Exception e) {
			return false;
		}
	}

	public String getUser(int nr) {
		Iterator<String> iterator = usersAndModels.keySet().iterator();

		for(int i=0; i<=nr; i++) {
			if(iterator.hasNext() && i==nr)
				return iterator.next();
			iterator.next();
		}

		return null;
	}

	public Vector<String> getAllUsers() {
		if(usersAndModels == null)
			return null;

		Iterator<String> iterator = usersAndModels.keySet().iterator();
		Vector<String> temp = new Vector<String>();

		while(iterator.hasNext())
			temp.add(iterator.next());

		return temp;
	}

	// Settery i Gettery

	public static ImageIcon getMinimizeIconOff() {
		return minimizeIconOff;
	}

	public static ImageIcon getMaximizeIconOff() {
		return maximizeIconOff;
	}

	public Image getBreakOrnament() {
		return breakOrnament;
	}

	public static ImageIcon getExitIconOn() {
		return exitIconOn;
	}

	public static ImageIcon getMinimizeIconOn() {
		return minimizeIconOn;
	}

	public static ImageIcon getMaximizeIconOn() {
		return maximizeIconOn;
	}

	public static ImageIcon getExitIcon() {
		return exitIcon;
	}

	public static ImageIcon getMinimizeIcon() {
		return minimizeIcon;
	}

	public static ImageIcon getMaximizeIcon() {
		return maximizeIcon;
	}

	public static ImageIcon getSearchIcon() {
		return searchIcon;
	}

	public ImageIcon getArrowIcon() {
		return arrowIcon;
	}

	public static ImageIcon getButtonIcon() {
		return buttonIcon;
	}

	public Image getOrnamentHorizontal() {
		return ornamentHorizontal;
	}

	public Image getOrnamentVertical() {
		return ornamentVertical;
	}

	public GBC getGBCparameter() {
		return GBCparameter;
	}

	public void setGBCparameter(GBC GBCparameter) {
		this.GBCparameter = GBCparameter;
	}

	public void setMaximumSize(int x, int y) {
		this.setMaximumSize(new Dimension(x, y));
	}

	public void setMinimumSize(int x, int y) {
		this.setMinimumSize(new Dimension(x, y));
	}

	public void setPrefferedSize(int x, int y) {
		this.setPreferredSize(new Dimension(x, y));
	}

	public void setFixedSize(int x, int y) {
		setMaximumSize(x, y);
		setMinimumSize(x, y);
		setPrefferedSize(x, y);
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}

	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}
}
