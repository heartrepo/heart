package heart.gui;

import heart.gui.heartComponents.*;
import heart.logic.request.RequestGetlistModel;
import heart.logic.response.RespType;
import heart.logic.response.ResponseGetlistModel;

import heart.logic.request.*;
import heart.logic.response.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:21
 */

// Całość znowu oparta na GridBagLayout

public class FrameModelExplorer extends MainWindow {
	final HeartJButton buttonGetModelList = new HeartJButton("GET MODEL LIST", HeartJButton.NORMAL);
	final HeartJButton buttonAddState = new HeartJButton("ADD STATE TO CURRENT", HeartJButton.NORMAL);
	final HeartJButton buttonRemoveState = new HeartJButton("REMOVE STATE FROM CURRENT", HeartJButton.NORMAL);
	final HeartJButton searchButton = new HeartJButton(HeartJButton.SEARCH);
	final HeartJTextField searchField = new HeartJTextField(" Search model...");
	static public final HeartJList modelList = new HeartJList();

	public FrameModelExplorer() {
		setMaxX(150);
		setFixedSize(getMaxX(),getMinY());
		setGBCparameter(new GBC(2,2).setWeight(0,100).setFill(GBC.BOTH).setIpad(getMaxX(),getMinY()));

		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(3, 5, 5, 15));

		modelList.setCellRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

				if(isSelected)
					label.setText((String) value + " (current)");

				label.setIcon(getArrowIcon());
				return label;
			}
		});

		add(new HeartHeader("Model Explorer"), new GBC(0, 0, 2, 1).setWeight(0, 0));
		add(buttonGetModelList, new GBC(0,1,2,1).setWeight(0, 0).setFill(GBC.BOTH).setInset(10, 15, 3, 0));
		add(buttonAddState, new GBC(0,2,2,1).setWeight(0, 0).setFill(GBC.BOTH).setInset(0, 15, 3, 0));
		add(buttonRemoveState, new GBC(0,3,2,1).setWeight(0, 0).setFill(GBC.BOTH).setInset(0, 15, 10, 0));
		add(searchField, new GBC(0,4,1,1).setWeight(0,0).setFill(GBC.BOTH).setIpad(250, 0).setInset(0, 0, 6, 0));
		add(searchButton, new GBC(1,4,1,1).setWeight(0,0).setFill(GBC.BOTH).setIpad(30, 0).setInset(0, 0, 6, 0));
		add(modelList.getScrollPane(), new GBC(0,5,2,1).setWeight(100, 100).setFill(GBC.BOTH));

		addLogic();

		if(debugMode)
			setBackground(Color.CYAN);
	}

	// LOGIC SECTION

	void addLogic() {
		modelList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {

				if (currentModelNr != -1 && !modelList.isSelectedIndex(currentModelNr)) {
					try {
						currentModelNr = modelList.getSelectedIndex();
						updateCurrentModel();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		final HeartJButton cancel = new HeartJButton("CANCEL", HeartJButton.NORMAL);

		buttonGetModelList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					// TODO: wyświetlanie Getlist działa jak na razie tylko w konsoli

					RequestGetlistModel getList = new RequestGetlistModel();
					ResponseGetlistModel respGetList = (ResponseGetlistModel) connectionHandler.sendAndGet(getList, RespType.MODEL_GETLIST);

					FrameConsole.updateJTestPane(getList, respGetList);
				} catch (Exception e1) {
					final HeartFramePopUp popUp = new HeartFramePopUp(new Dimension(200,100), " GET MODEL LIST");
					final JPanel panelInsidePopUp = new JPanel();

					panelInsidePopUp.setLayout(new GridBagLayout());
					panelInsidePopUp.setBackground(new Color(239, 238, 243));

					HeartJLabel l = new HeartJLabel("Error. Connect first.");
					l.setForeground(Color.RED);
					panelInsidePopUp.add(l, new GBC(0, 0).setWeight(100, 100).setFill(GBC.BOTH));
					panelInsidePopUp.add(cancel, new GBC(0, 1).setWeight(100, 100).setFill(GBC.BOTH));

					cancel.setHorizontalAlignment(SwingConstants.CENTER);
					cancel.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent event) {
							popUp.setVisible(false);
							popUp.dispose();
						}
					});

					popUp.addPanel(panelInsidePopUp);

					popUp.setVisible(true);
				}
			}
		});

		final HeartJButton addState = new HeartJButton("ADD STATE", HeartJButton.NORMAL);
		final HeartJTextField stateName = new HeartJTextField("");
		final HeartJTextPane stateDef = new HeartJTextPane().getEditable();

		buttonAddState.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final HeartFramePopUp popUp = new HeartFramePopUp(new Dimension(450,350), " ADD STATE TO CURRENT MODEL");
				final JPanel panelInsidePopUp = new JPanel();

				panelInsidePopUp.setLayout(new GridBagLayout());
				panelInsidePopUp.setBackground(new Color(239, 238, 243));

				final HeartJLabel stateNameLabel = new HeartJLabel("State name:");
				final HeartJLabel stateDefLabel = new HeartJLabel("State definition:");

				if(getAllUsers()!=null) {
					addState.setHorizontalAlignment(SwingConstants.CENTER);
					addState.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO: Obsługa dodawania do tymczasowego modelu

							try {
								updateCurrentModel();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							addState.removeActionListener(this);

							popUp.setVisible(false);
							popUp.dispose();
						}
					});

					panelInsidePopUp.add(stateNameLabel, new GBC(0, 0).setWeight(0, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(stateName, new GBC(1, 0).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(stateDefLabel, new GBC(0, 1).setWeight(0, 100).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(stateDef, new GBC(1, 1).setWeight(100, 100).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(addState, new GBC(0, 2).setWeight(0, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(cancel, new GBC(1, 2).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
				}
				else {
					popUp.setSize(new Dimension(200,100));
					HeartJLabel l = new HeartJLabel("Error. Connect first.");
					l.setForeground(Color.RED);
					panelInsidePopUp.add(l, new GBC(0, 0).setWeight(100, 100).setFill(GBC.BOTH));
					panelInsidePopUp.add(cancel, new GBC(0, 1).setWeight(100, 100).setFill(GBC.BOTH));
				}

                addState.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                       try {
                           if(!(stateDef.getText().length() == 0) &&
                             !(stateName.getText().length() == 0) &&
                             !(ModelAddContent.modelName2Text.getText().length() == 0)) {

                                Request req = new RequestAddState(
                                        ModelAddContent.userName2Text.getText(),
                                        ModelAddContent.modelName2Text.getText(),
                                        stateName.getText(),
                                        stateDef.getText()
                                );

                                Response resp = MainWindow.connectionHandler.sendAndGet(
                                    req,
                                    RespType.STATE_ADD
                                );

                                FrameConsole.updateJTestPane(req, resp);

                                if(resp instanceof ResponseError)
                                    JOptionPane.showMessageDialog(null,
                                       ((ResponseError)resp).getRaw());
                                else {
                                    JOptionPane.showMessageDialog(null,
                                       "State added to the current model.");
                                }

                                stateName.setText("");
                                stateDef.setText("");
                                popUp.setVisible(false);
                                popUp.dispose();
                           } else {
                                JOptionPane.showMessageDialog(null,
                                        "All fields must be fiiled correctly.\n" +
                                "(or current model is empty)");
                           }
                       } catch(Exception ignoreForNow) {}
                    }
                });

				cancel.setHorizontalAlignment(SwingConstants.CENTER);
				cancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						popUp.setVisible(false);
						popUp.dispose();
					}
				});

				popUp.addPanel(panelInsidePopUp);

				popUp.setVisible(true);
			}
		});

		final HeartJButton removeState = new HeartJButton("REMOVE STATE", HeartJButton.NORMAL);

		buttonRemoveState.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final HeartFramePopUp popUp = new HeartFramePopUp(new Dimension(300,100), " REMOVE STATE FROM CURRENT MODEL");
				final JPanel panelInsidePopUp = new JPanel();

				panelInsidePopUp.setLayout(new GridBagLayout());
				panelInsidePopUp.setBackground(new Color(239, 238, 243));

				final HeartJLabel stateNameLabel = new HeartJLabel("State name:");

				if(getAllUsers()!=null) {
					addState.setHorizontalAlignment(SwingConstants.CENTER);
					addState.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO: Obsługa usuwania z tymczasowego modelu

							try {
								updateCurrentModel();
							} catch (IOException e1) {
								e1.printStackTrace();
							}

							addState.removeActionListener(this);

							popUp.setVisible(false);
							popUp.dispose();
						}
					});

					panelInsidePopUp.add(stateNameLabel, new GBC(0, 0).setWeight(0, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(stateName, new GBC(1, 0).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(removeState, new GBC(0, 2).setWeight(0, 0).setFill(GBC.BOTH).setAllInsets(5));
					panelInsidePopUp.add(cancel, new GBC(1, 2).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
				}
				else {
					popUp.setSize(new Dimension(200,100));
					HeartJLabel l = new HeartJLabel("Error. Connect first.");
					l.setForeground(Color.RED);
					panelInsidePopUp.add(l, new GBC(0, 0).setWeight(100, 100).setFill(GBC.BOTH));
					panelInsidePopUp.add(cancel, new GBC(0, 1).setWeight(100, 100).setFill(GBC.BOTH));
				}

                removeState.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            if(!(stateName.getText().length() == 0) &&
                                    !(ModelAddContent.userName2Text.getText().length() == 0)) {
                                Request req = new RequestRemoveState(
                                        ModelAddContent.userName2Text.getText(),
                                        ModelAddContent.modelName2Text.getText(),
                                        stateName.getText()
                                );

                                Response resp = MainWindow.connectionHandler.sendAndGet(
                                    req,
                                    RespType.STATE_REMOVE
                                );

                                FrameConsole.updateJTestPane(req, resp);

                                if(resp instanceof ResponseError) {
                                    JOptionPane.showMessageDialog(null,
                                            ((ResponseError)resp).getRaw());
                                } else {
                                    JOptionPane.showMessageDialog(null,
                                            "State removed from the current model.");
                                }

                                stateName.setText("");
                                popUp.setVisible(false);
                                popUp.dispose();
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "All fields must be fiiled correctly.\n" +
                                                "(or current model is empty)");
                            }
                        } catch(Exception ignoreForNow) {}
                    }
                });

				cancel.setHorizontalAlignment(SwingConstants.CENTER);
				cancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						popUp.setVisible(false);
						popUp.dispose();
					}
				});

				popUp.addPanel(panelInsidePopUp);

				popUp.setVisible(true);
			}
		});
	}
}
