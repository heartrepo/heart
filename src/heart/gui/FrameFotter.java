package heart.gui;

import heart.gui.heartComponents.HeartJLabel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 07.02.13
 * Time: 16:32
 */

// Stopka
// TODO: Listener zmieniający stan w zalezności od jakiegoś pola które wskazuje czy jest error czy nie

public class FrameFotter extends MainWindow {
	Color backgroundReadyColor = new Color(0, 122, 205);
	Color backgroundErrorColor = new Color(201, 81, 0);
	String labelReadyDescription = "Ready";
	String labelErrorDescription = "Error";
	boolean isReady = true;

	HeartJLabel l;

	public FrameFotter() {

		// ustawienie maksymalnej wysokości elementu, wypełnienia, marginesów
		setMaxY(11); // wysokość 22 - liczone podwójnie
		setGBCparameter(new GBC(0, 4, 3, 1).setWeight(100, 0).setFill(GBC.BOTH).setIpad(getMinX(), getMaxY()));
		setFixedSize(getMaxX(),getMaxY());
		setBorder(new EmptyBorder(0, 5, 3, 10));

		setBackground(backgroundReadyColor);
		setLayout(new BorderLayout());

		l = new HeartJLabel(labelReadyDescription);
		l.setForeground(Color.WHITE);

		add(l, BorderLayout.WEST);

		if(debugMode)
			setBackground(Color.YELLOW);
	}

	// zwraca informację czy jest wyświetlany błąd czy nie
	boolean isError() {
		return !isReady;
	}

	// zmienia stan panelu na przeciwny
	void switchState() {
		if(isReady) {
			setBackground(backgroundErrorColor);
			l.setText(labelErrorDescription);
		}
		else {
			setBackground(backgroundReadyColor);
			l.setText(labelReadyDescription);
		}

		isReady = !isReady;
		repaint();
	}
}
