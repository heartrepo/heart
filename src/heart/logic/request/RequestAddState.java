package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestAddState extends Request {
	String username;
	String modelname;
	String statename;
	String statedef;

	public RequestAddState(String username, String modelname, String statename, String statedef) {
		this.username = username;
		this.modelname = modelname;
		this.statename = statename;
		this.statedef = statedef;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}

	public String getStatename() {
		return statename;
	}

	public String getStatedef() {
		return statedef;
	}
}

