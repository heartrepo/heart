package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 16:09
 */

public class RequestRemoveState extends Request {
	String username;
	String modelname;
	String statename;

	public RequestRemoveState(String username, String modelname, String statename) {
		this.username = username;
		this.modelname = modelname;
		this.statename = statename;
	}

	public String getUsername() {
		return username;
	}

	public String getModelname() {
		return modelname;
	}

	public String getStatename() {
		return statename;
	}
}

