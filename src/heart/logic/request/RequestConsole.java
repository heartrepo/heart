package heart.logic.request;

/**
 * Created by: Piotrek
 * Date: 30.01.13
 * Time: 18:18
 */

public class RequestConsole extends Request {
	String rawRequest;

	public RequestConsole(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public String getRawRequest() {
		return rawRequest;
	}
}
