package heart.logic.response;

public class ResponseGetModel extends Response {
    private String model;

    public ResponseGetModel(String raw, String model) {
        super(raw);
        this.model = model;
    }

    public String getModelContent() {
        return model;
    }
}
