package heart.logic.response;

/**
 * Created with IntelliJ IDEA.
 * User: Andrzej
 * Date: 27.01.13
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */

//klasa potrzebna do powiedzenia parserowi,
//z jaką odpowiedzią typu String ma do czynienia, żeby
//wiedział potem, na jaki obiekt Response ją zamienić.
public enum RespType {
    MODEL_EXISTS,
    MODEL_RUN,
    MODEL_GETLIST,
    MODEL_GET,
    MODEL_VERIFY,
    MODEL_ADD,
    MODEL_REMOVE,
    STATE_ADD,
    STATE_REMOVE,
    HELLO
}
