package heart.logic.response;

//Klasa abstrakcyjna - abstrakcja odpowiedzi serwera
public abstract class Response {

    //Przechowywanie raw stringa z odpowiedzią, chcemy to appendować do
    //konsoli w gui
    protected String rawResponse;

    public Response(String raw) {
        rawResponse = raw;
    }

    public String getRaw() { return rawResponse; }
}
