package heart.logic.parsing;

import heart.logic.request.*;
import heart.logic.response.*;
import heart.logic.response.ResponseError;

import java.util.ArrayList;
import java.util.List;

public class Parser implements CommParser {

    //upchnąłem tę operację do osobnej metody bo jest dosyć powtarzalna
    protected List<String> stringToList(String raw) {
        if(raw.length() == 0)
            return new ArrayList<String>();

        String[] splitted = raw.split(",");

        for(int i = 0; i < splitted.length; i++) {
            while(splitted[i].startsWith("["))
                splitted[i] = splitted[i].substring(1, splitted[i].length());

            while(splitted[i].endsWith("]"))
                splitted[i] = splitted[i].substring(0, splitted[i].length() - 1);
        }

        List<String> resultList = new ArrayList<String>();
        for(int i = 0; i < splitted.length - 1; i += 2)
            resultList.add(splitted[i] + "," + splitted[i + 1]);

        return resultList;
    }

    public Response stringToResponse(String raw, RespType type) {
        System.out.println("MAM RAW " + raw);

        if(raw.contains("true") || raw.length() == 0) {

            if(type == RespType.HELLO) {
                String info = raw.substring(7, raw.length() - 2);
                return new ResponseHello(raw, info);
            }

            if(type == RespType.MODEL_GETLIST) {
                if(raw.length() > 0)
                    raw = raw.substring(7, raw.length() - 2);

                List<String> result = stringToList(raw);
                return new ResponseGetlistModel(raw, result);
            }

            if(type == RespType.STATE_ADD) {
                String tmpRaw = raw;
                if(raw.length() > 7)
                    raw = raw.substring(7, raw.length() - 2);
                List<String> result = stringToList(raw);
                return new ResponseAddState(tmpRaw, result);
            }

            if(type == RespType.MODEL_VERIFY) {
                String tmpRaw = raw;
                raw = raw.substring(7, raw.length() - 2);
                List<String> result = stringToList(raw);
                return new ResponseVerifyModel(tmpRaw, result);
            }

            if(type == RespType.MODEL_ADD)
                return new ResponseAddModel(raw);

            if(type == RespType.MODEL_REMOVE)
                return new ResponseRemoveModel(raw);

            if(type == RespType.STATE_REMOVE)
                return new ResponseRemoveState(raw);

            if(type == RespType.MODEL_GET) {
                String content = raw.substring(7, raw.length() - 2);
                return new ResponseGetModel(raw, content);
            }

            if(type == RespType.MODEL_EXISTS) {
                boolean exists;

                if(raw.contains("true]"))
                    exists = true;
                else
                    exists = false;

                return new ResponseExistsModel(raw, exists);
            }

            if(type == RespType.MODEL_RUN) {
                String tmp = raw.substring(6, raw.length());
                String list = tmp.split("]],")[0];
                String str = tmp.split("]],")[1];
                String strPart = str.substring(1, str.length() - 2);
                List<String> listPart = stringToList(list);

                return new ResponseRunModel(raw, listPart, strPart);
            }

        } else if(raw.contains("false,")) {
            String errorMsg = raw.substring(7, raw.length() - 2);
            return new ResponseError(errorMsg);
        }

        //nie rozpoznalem odpowiedzi - pole do popisu przy rozszerzaniu protokołu
        return null;
    }

    //Robota dla tjebja
    public String RequestToString(Request req) {

	    // [hello, +client_name].
	    if(req instanceof RequestHello) {
		    RequestHello temp = (RequestHello) req;
		    String out = "[hello, '"
				    + temp.getUsername() + "'].";

		    req.setRawRequest(out);
		    return out;
	    }

	    // [model, getlist].
	    if(req instanceof RequestGetlistModel) {
		    String out = "[model,getlist].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [model, get, +format, +modelname, +username, +[+parts]].
	    if(req instanceof RequestGetModel) {
		    RequestGetModel temp = (RequestGetModel) req;
		    String out = "[model,get,"
				    + temp.getFormat() + ",'"
				    + temp.getModelname() + "','"
				    + temp.getUsername() + "',[";

		    for(String s : temp.getParts())
		        out += ( "[" + s + "],");

		    out = out.substring(0, out.length()-2);
		    out += "]]].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [model,exists,+modelname, +username].
	    if(req instanceof RequestExistModel) {
		    RequestExistModel temp = (RequestExistModel) req;
		    String out = "[model, exists,'"
				    + temp.getModelname() + "','"
				    + temp.getUsername() + "'].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [model, add, +format, +modelname, +username, +'CONTENT_OF_THE_MODEL_IN_SPECIFIED_FORMAT' ].
	    if(req instanceof RequestAddModel) {
		    RequestAddModel temp = (RequestAddModel) req;
		    String out = "[model, add, "
				    + temp.getFormat() + ", '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "', '"
				    + temp.getModel() + "'].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [model,remove, +modelname, +username].
	    if(req instanceof RequestRemoveModel) {
		    RequestRemoveModel temp = (RequestRemoveModel) req;
		    String out =  "[model, remove, '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "'].";

  	        req.setRawRequest(out);
		    return out;
	    }

	    // [model,run, +modelname, +username, +{ddi | gdi | tdi | foi}, +tables, +{statename | statedef} ].
	    if(req instanceof RequestRunModel) {
		    RequestRunModel temp = (RequestRunModel) req;
		    String out = "[model, run, '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "', "
				    + temp.getMode() + ", "
				    + temp.getTables() + ", "
				    + temp.getStatenameORstatedef() + "].";


			req.setRawRequest(out);
		    return out;
	    }

	    // [state, add ,+modelname, +username, +statename, +statedef ].
	    if(req instanceof RequestAddState) {
		    RequestAddState temp = (RequestAddState) req;
		    String out = "[state, add, '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "', "
				    + temp.getStatename() + ", "
				    + temp.getStatedef() + "].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [state, remove, +modelname, +username, +statename ].
	    if(req instanceof RequestRemoveState) {
		    RequestRemoveState temp = (RequestRemoveState) req;
		    String out = "[state, remove, '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "', "
				    + temp.getStatename() + "].";

			req.setRawRequest(out);
		    return out;
	    }

	    // [model, verify, +{vcomplete | vcontradict | vsubsume | vreduce}, +modelname, +username, +schema].
	    if(req instanceof RequestVerifyModel) {
		    RequestVerifyModel temp = (RequestVerifyModel) req;
		    String out = "[model, verify, "
				    + temp.getMode() + ", '"
				    + temp.getModelname() + "', '"
				    + temp.getUsername() + "', "
				    + temp.getSchema() + "].";

			req.setRawRequest(out);
		    return out;
	    }

	    // specified string from console
	    if(req instanceof RequestConsole) {
		    RequestConsole temp = (RequestConsole) req;
		    String out = temp.getRawRequest();

			req.setRawRequest(out);
		    return out;
	    }

       return null;
    }
}